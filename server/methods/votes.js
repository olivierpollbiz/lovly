Meteor.methods({

  pollBizVotesSetSeen: function(id) {
    const u = Meteor.user();
    if(!u) return false;

    return Votes.update(id, {$set: {seen: true}});
  },

});

