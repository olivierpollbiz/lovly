import {Meteor} from "meteor/meteor";

Template.pollbizAdminHrUser.onCreated(function() {
  this.fromDate = new ReactiveVar(0);
  this.toDate = new ReactiveVar(0);
});

Template.pollbizAdminHrUser.onRendered(function() {
  const $calendar = $(".datepicker");
  $calendar.datepicker();
  $calendar.datepicker("option", $.datepicker.regional['en']);
  $calendar.datepicker("option", "dateFormat", "yy-mm-dd");
});

Template.pollbizAdminHrUser.helpers({

  users: function() {
    return Meteor.users.find({}, {sort: {"profile.company.name": 1, "profile.obName.last": 1}});
  },

  voteReceveid: function() {
    const query = {"to._id": this._id};
    const fromDate = Template.instance().fromDate.get();
    if(fromDate) query.votedAt = {$gt: new Date(fromDate)};
    const toDate = Template.instance().toDate.get();
    if(toDate) {
      if(fromDate) query.votedAt = {$gt: new Date(fromDate), $lt: new Date(toDate)};
      else query.votedAt = {$lt: new Date(toDate)};
    }
    return Votes.find(query).count();
  },

  voteAnswered: function() {
    const query = {"from._id": this._id};
    const fromDate = Template.instance().fromDate.get();
    if(fromDate) query.votedAt = {$gt: new Date(fromDate)};
    const toDate = Template.instance().toDate.get();

    Session.set('postSubmitErrors', {});
    if(toDate) {
      if(fromDate) query.votedAt = {$gt: new Date(fromDate), $lt: new Date(toDate)};
      else query.votedAt = {$lt: new Date(toDate)};
      if(new Date(toDate).getTime() < new Date(fromDate).getTime()) return Session.set('postSubmitErrors', {dateTo: "to date must be greater than from date"});
    }

    return Votes.find(query).count();
  },

  tags: function() {
    const query = {"to._id": this._id};
    const fromDate = Template.instance().fromDate.get();
    if(fromDate) query.votedAt = {$gt: new Date(fromDate)};
    const toDate = Template.instance().toDate.get();
    if(toDate) {
      if(fromDate) query.votedAt = {$gt: new Date(fromDate), $lt: new Date(toDate)};
      else query.votedAt = {$lt: new Date(toDate)};
    }

    const tags = [];
    const votes = Votes.find(query).fetch();
    for(let i = 0; i < votes.length; i++) {
      const vote = votes[i];
      const t = vote.question.tags;
      if(t) {
        for(let j = 0; j < t.length; j++) {
          const tt = t[j];
          // console.log(tt);
          const i = orbiter.core.arrayObjectIndexOf(tags, tt, 'name');
          if(i === -1)
            tags.push({name: tt, value: 1});
          else {
            const nb = tags[i].value + 1;
            tags[i] = {name: tt, value: nb};
          }
        }
      }
    }
    // console.log(tags);
    return tags
  },

  hasCompany: function() {
    return Companies.findOne({hrId: Meteor.userId()});
  },
});


Template.pollbizAdminHrUser.events({

  'change #dateFromInput': function(e, tpl) {
    tpl.fromDate.set($(e.currentTarget).val());
  },

  'change #dateToInput': function(e, tpl) {
    tpl.toDate.set($(e.currentTarget).val());
  },
});