/**
 * Created by David FAIVRE-MAÇON on 28/11/2017.
 */

Template.pollbizAdminHrQuestions.onCreated(function() {
  this.questionEditId = new ReactiveVar(false);
});

Template.pollbizAdminHrQuestions.helpers({

  questions() {
    return Questions.find({}, {sort: {question: 1}});
  },

  isEditing: function() {
    return Template.instance().questionEditId.get();
  },

  hasCompany: function() {
    return Companies.findOne({hrId: Meteor.userId()});
  },

});

Template.pollbizAdminHrQuestions.events({

  'click .edit': function(e, tpl) {
    tpl.questionEditId.set(this._id);
  },

  'click .delete': function() {
    const that = this;
    const translation = TAPi18n.__('deleteConfirmationMessage');
    bootbox.confirm(translation, function(result) {
      if(!result) return;
      Meteor.call('hrQuestionsRemove', that._id);
    });
  },

  'submit #import': function(e, tpl) {
    e.preventDefault();
    const lines = $('textarea[name=import]').val().split('\n');
    Meteor.call('hrQuestionsInsert', lines, function(err) {
      if(!err) {
        $('textarea[name=import]').val('');
        orbiter.core.displayMessage('Added questions');
      }
    });
  }

  // 'submit #import': function(e, tpl) {
  //   e.preventDefault();
  //   const question = $('input[name=import]').val();
  //
  //   if(!question.trim()) return;
  //   Meteor.call('hrQuestionsInsert', question, function(err) {
  //     if(!err) {
  //       $('input[name=import]').val('');
  //       orbiter.core.displayMessage('Added question');
  //     }
  //   });
  // }
});
