import {Meteor} from "meteor/meteor";

Meteor.publish("allQuestions", function() {
  if(!orbiter.accounts.isAdmin()) return [];
  else return Questions.find();
});

Meteor.publish("allTagsMyManagedCompany", function() {
  return Tags.find({companyId: pollBiz.getManagedCompanyId()});
});

Meteor.publish("votesForMe", function() {
  const u = Meteor.user();
  if(!u) return [];
  else return Votes.find({"to._id": u._id});
});

Meteor.publish("voteAnswer", function(vote_id) {
  return Votes.find({"_id": vote_id});
});

Meteor.publish("pollbiz-admin-companies", function(filterName, options) {
  const u = Meteor.user();
  if(!u || !Roles.userIsInRole(u, ['webmaster', 'admin'])) return [];
  else {
    const regex = new RegExp(filterName, 'i');
    options = options || {};
    options.sort = {name: 1};
    return Companies.find({name: {$regex: regex}}, options);
  }
});

Meteor.publish("players", function() {
  const u = Meteor.user();
  if(!u || !u.profile || !u.profile.company || !u.profile.company._id) return [];
  return Meteor.users.find({"profile.company._id": u.profile.company._id}, {fields: {"profile.obName.first": 1, "profile.obName.last": 1, "profile.company": 1}});
});

Meteor.publish('users', function() {
  const uid = this.userId;
  const u = Meteor.users.findOne(uid);
  if(!u) return [];

  if(Roles.userIsInRole(u, ['webmaster'])) return Meteor.users.find({});

  if(Roles.userIsInRole(u, ['admin']))
    return Meteor.users.find({roles: {$ne: 'webmaster'}});
  else
    return Meteor.users.find({_id: uid}, {fields: {status: 0}});
});


Meteor.publish("autocompleteCompany", function(selector, options) {
  Autocomplete.publishCursor(Companies.find(selector, options), this);
  this.ready();
});

// hr

Meteor.publish("pollbiz-admin-hr-companies", function() {
  if(!pollBiz.isHr(this.userId)) return [];

  return Companies.find({hrId: this.userId});
});

Meteor.publish("pollbiz-admin-hr-questions", function() {
  if(!pollBiz.isHr()) return [];
  else {
    const u = Meteor.user();
    // const myCompanyId = (u.profile && u.profile.company) ? u.profile.company._id : null;
    const company = Companies.findOne({hrId: u._id});
    if(!company) return [];
    //FIXME et si j'ai plusieurs company?
    return Questions.find({companyId: company._id});
  }
});

Meteor.publish("users-hr-votes", function() {
  if(!pollBiz.isHr(this.userId)) return [];

  // const u = Meteor.user();
  // const company = Companies.findOne({hrId: u._id});
  const companyId = pollBiz.getManagedCompanyId();
  if(companyId) return Votes.find({companyId: companyId});
  else return []
});

Meteor.publish("admin-votes-user", function(userId) {
  if(!orbiter.accounts.isAdmin()) return [];

  // const u = Meteor.user();
  // const company = Companies.findOne({hrId: u._id});
  if(userId) return Votes.find({$or: [{"to._id": userId}, {"from._id": userId}]});
  else return []
});
Meteor.publish("users-hr-company", function(hrId) {
  if(!orbiter.accounts.isAdmin(this.userId)) return [];
  return Companies.find({hrId: hrId});
});

Meteor.publish('users-hr', function() {
  if(!pollBiz.isHr(this.userId)) return [];

  // const myCompanyId = Meteor.user().profile.company._id;
  // console.log(Meteor.user());
  const u = Meteor.user();
  const company = Companies.findOne({hrId: u._id});
  if(!company) return [];
  // filter by myCompanies
  return Meteor.users.find({"profile.company._id": company._id}, {fields: {status: 0}});
});


// ALLOW

Meteor.publish(null, function() {
  return OrbiterMedias.find({});
});

// Meteor.publish("media_id", function(media_id) {
//   return OrbiterMedias.find({_id: media_id});
// });

if(typeof(OrbiterMedias) !== "undefined") {
  OrbiterMedias.allow({
    'insert': function(userId, fileObj) {
      return true;
    },

    'update': function(userId, fileObj) {
      return true;
    },

    remove: function(userId, fileObj) {
      return true;
    },

    download: function(userId, fileObj) {
      return true;
    }
  });

// FIXME: can not published all cropped
  Meteor.publish(null, function() {
    return OrbiterMediasCropped.find({});
  });

  OrbiterMediasCropped.allow({
    'insert': function(userId, fileObj) {
      return true;
    },

    'update': function(userId, fileObj) {
      return true;
    },

    remove: function(userId, fileObj) {
      return true;
    },

    download: function(userId, fileObj) {
      return true;
    }
  });
}