Meteor.methods({

  pollBizAnswerSend: function(answer) {
    const u = Meteor.user();
    if(!u) return false;

    // console.log(answer);

    const playerSelected = Meteor.users.findOne(answer.player_id);
    // FIXME retur error
    if(!playerSelected) return false;
    const email = orbiter.accounts.getEmailAddress(playerSelected);
    const fullName = pollBiz.getFullName(playerSelected);
    // const message = "A colleague from <strong>" + u.profile.company.name + "</strong> has voted you as: <br><strong>" + answer.question.question + "</strong>";
    const subject = 'Tu viens de recevoir un vote';

    const companyId = (playerSelected.profile && playerSelected.profile.company) ? playerSelected.profile.company._id : null;

    // TO
    const vote_id = Votes.insert({
      to: {
        _id: playerSelected._id,
        fullName: fullName
      },
      question: answer.question,
      companyId: companyId,
      players: answer.players,
      votedAt: new Date()
    });

    // non compatible avec les filtre dates !
    // console.log(answer.question.tags);
    // if(answer.question.tags && answer.question.tags.length) {
    //   const query = {};
    //   for(let i = 0; i < answer.question.tags.length; i++) {
    //     const tag = answer.question.tags[i];
    //     query["tags." + tag] = 1;
    //   }
    //   console.log(playerSelected._id);
    //   console.log(query);
    //   if(Object.keys(query).length) Meteor.users.update(playerSelected._id, {$inc: query});
    // }

    // FROM
    Votes.insert({
      from: {
        _id: u._id,
        fullName: pollBiz.getFullName(u),
      },
      companyId: companyId,
      votedAt: new Date()
    });

    const templateContent = Assets.getText('mail-template-new-vote.html');
    const templateName = 'mailTemplateNewVote';
    SSR.compileTemplate(templateName, templateContent);
    const mailHTMLContent = SSR.render(templateName, {absoluteUrl: Meteor.absoluteUrl(), appName: orbiter.core.appName, vote_id: vote_id});
    // const mailHTMLContent = SSR.render(templateName, {});
    const SMTPSettings = Meteor.settings.smtp;
    return Email.send({
      from: SMTPSettings.from,
      to: email,
      subject: subject,
      html: mailHTMLContent
    });
  },

});
