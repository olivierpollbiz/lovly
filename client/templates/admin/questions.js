/**
 * Created by David FAIVRE-MAÇON on 28/11/2017.
 */

import {ReactiveVar} from 'meteor/reactive-var';

Template.pollbizAdminQuestions.onCreated(function() {
  this.questionEditId = new ReactiveVar(0);
});

Template.pollbizAdminQuestions.helpers({

  questions() {
    return Questions.find({companyId: {$exists: false}}, {sort: {createdAt: 1}});
  },

  isEditing: function() {
    return Template.instance().questionEditId.get();
  },

});

Template.pollbizAdminQuestions.events({

  'click .edit': function(e, tpl) {
    tpl.questionEditId.set(this._id);
  },

  'click .delete': function() {
    const that = this;
    const translation = TAPi18n.__('deleteConfirmationMessage');
    bootbox.confirm(translation, function(result) {
      if(!result) return;
      Meteor.call('adminQuestionsRemove', that._id);
    });
  },

  'submit #import': function(e, tpl) {
    e.preventDefault();
    const lines = $('textarea[name=import]').val().split('\n');
    Meteor.call('adminQuestionsImport', lines, function(err) {
      if(!err) {
        $('textarea[name=import]').val('')
      }
    });
  }
});

/**
 * Edit
 */

Template.pollbizAdminQuestionEdit.onCreated(function() {
  this.questionEditId = Blaze.currentView.parentView.parentView.templateInstance().questionEditId;
});


Template.pollbizAdminQuestionEdit.helpers({

  editedQuestion() {
    return Questions.findOne(Template.instance().questionEditId.get());
  },
});


Template.pollbizAdminQuestionEdit.events({

  'click .cancel': function(e, tpl) {
    tpl.questionEditId.set(0);
  },

  'submit': function(e, tpl) {
    e.preventDefault();

    let form = $('form').serializeJSON();
    Meteor.call('adminQuestionsUpdate', this._id, form, function(err) {
      if(!err) tpl.questionEditId.set(0)
    });
  }
});