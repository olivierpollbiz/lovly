/**
 * Created by David FAIVRE-MAÇON on 28/11/2017.
 */

// NAMESPACE
if(typeof (pollBiz) === "undefined") pollBiz = {};

pollBiz.isHr = function(userId) {
  userId = userId || Meteor.userId();
  return (Roles.userIsInRole(userId, ['hr', 'admin', 'superAdmin', 'webmaster']));
};

pollBiz.isOnlyHr = function(userId) {
  userId = userId || Meteor.userId();
  return (Roles.userIsInRole(userId, ['hr']));
};

pollBiz.isOnlyHrStrict = function(userId) {
  userId = userId || Meteor.userId();
  const otherHr = Roles.userIsInRole(userId, [, 'admin', 'superAdmin', 'webmaster']);
  return (!otherHr && Roles.userIsInRole(userId, ['hr']));
};
pollBiz.profileCompleted = function() {
  const u = Meteor.user();
  return u && u.profile && u.profile.company
};

pollBiz.getFirstName = function(u) {
  u = u || Meteor.user();
  return pollBiz.capitalizeFirstLetter(u.profile.obName.first);
};

pollBiz.getLastName = function(u) {
  u = u || Meteor.user();
  return u.profile.obName.last.toUpperCase()
};

pollBiz.getFullName = function(u) {
  u = u || Meteor.user();
  return u.profile.obName.first + " " + u.profile.obName.last
};

pollBiz.getCompanyId = function(u) {
  u = u || Meteor.user();
  return (u.profile && u.profile.company) ? u.profile.company._id : null;
};

pollBiz.getManagedCompanyId = function(u) {
  u = u || Meteor.user();
  const c = Companies.findOne({hrId: u._id});
  return c ? c._id : false;
};

pollBiz.randomFromCollection = function(C, query) {
  // console.log(query);
  let c = [];
  if(!query) {
    // console.log('y');
    query = {companyId: {$exists: false}};
    c = C.find(query).fetch();
  }
  else {
    c = C.find(query).fetch();
    if(!c.length) {
      // console.log('n');
      query = {companyId: {$exists: false}};
      c = C.find(query).fetch();
    }
  }

  // console.log(c);
  const i = pollBiz.randomInRangeMaxExclusive(0, c.length);
  return c[i]
};

pollBiz.randomInRangeMaxExclusive = function(min, max) {
  return Math.floor(Math.random() * (max - min)) + min;
};

pollBiz.randomInRangeMaxInclusive = function(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
};

pollBiz.capitalizeFirstLetter = function(string) {
  return string.charAt(0).toUpperCase() + string.slice(1);
};

