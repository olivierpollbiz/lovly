Meteor.methods({

  profileUpdate: function(u) {
    if(!this.userId)
      throw new Meteor.Error("not-logged-in", "Must be logged in");

    // if(!u.first) return orbiter.core.error('First Name Required', ['profile']);
    // if(!u.last) return orbiter.core.error('Last Name Required', ['profile']);
    if(!u.companyNew && !u.companyId) return orbiter.core.error('Company Required', ['profile']);

    let profileCompany = {};
    if(u.companyId) {
      const company = Companies.findOne(u.companyId);
      if(company) {
        profileCompany = {
          _id: company._id,
          name: company.name
        }
      }
    }
    else if(u.companyNew) {
      const company = Companies.findOne({
        name: {
          "$regex": "^" + u.companyNew + "\\b", "$options": "i"
        }
      });
      if(company) {
        profileCompany = {
          _id: company._id,
          name: company.name
        }
      }
      else {
        const user = Meteor.user();
        const newCompanyId = Companies.insert({
          name: u.companyNew,
          createdAt: new Date(),
          createdBy: {
            _id: user._id,
            name: pollBiz.getFullName(user)
          },
        });
        profileCompany = {
          _id: newCompanyId,
          name: u.companyNew
        }
      }
    }
    else return orbiter.core.error('Company name error', ['profile']);

    // check number of company players and send mail if >= 4
    const otherPlayersCompany = Meteor.users.find({"_id": {$ne: this.userId}, "profile.company._id": profileCompany._id}).fetch();
    if(otherPlayersCompany.length === 3) {
      // send mail to other player of company
      pollBiz.sendMailPlayersCompanyReached(otherPlayersCompany, u);
    }
    else if(otherPlayersCompany.length) {
      // send mail to other player of company
      pollBiz.sendMailPlayersCompanyJoined(otherPlayersCompany, u);
    }

    return Meteor.users.update(this.userId, {
      $set: {
        // "profile.obName.first": u.first,
        // "profile.obName.last": u.last,
        "profile.company": profileCompany,
      }
    });
  },

  checkSendMailToOther: function(userOld) {
    if(!this.userId)
      throw new Meteor.Error("not-logged-in", "Must be logged in");

    const userUpdated = Meteor.users.findOne(userOld._id);

    // Si l'entreprise est la même ne rien faire
    if(userOld.profile.company && userOld.profile.company._id === userUpdated.profile.company._id) return;

    const userName = {
      first: pollBiz.getFirstName(userUpdated),
      last: pollBiz.getLastName(userUpdated)
    };

    const otherPlayersCompany = Meteor.users.find({"_id": {$ne: userOld._id}, "profile.company._id": userUpdated.profile.company._id}).fetch();
    if(otherPlayersCompany.length)
      pollBiz.sendMailPlayersCompanyJoined(otherPlayersCompany, userName)
  },

  pollBizInviteColleagueSend: function(emails) {
    if(!this.userId)
      throw new Meteor.Error("not-logged-in", "Must be logged in");

    const u = Meteor.user();
    if(!u.profile.company || !u.profile.company._id) return;

    //FIXME les player peuvent déjà appartenir a une autre company
    const playersCompany = Meteor.users.find({"profile.company._id": u.profile.company._id}, {fields: {registered_emails: 1}}).fetch().map(function(p) {
      return p.registered_emails[0].address
    });
    const emails_filtered = emails.filter(function(i) {return playersCompany.indexOf(i) < 0;});

    if(emails_filtered.length) {
      const fromUser = {
        _id: u._id,
        companyName: u.profile.company.name,
        first: pollBiz.getFirstName(u),
        last: pollBiz.getLastName(u)
      };
      pollBiz.sendMailInviteColleague(emails_filtered, fromUser)
    }
  }
});
