/**
 * Created by David FAIVRE-MAÇON on 28/11/2017.
 */

// NAMESPACE
if(typeof (pollBiz) === "undefined") pollBiz = {};

pollBiz.sendMailPlayersCompanyReached = function(otherPlayersCompany) {
  // Est appelé quand un player s'inscrit
  if(otherPlayersCompany.length !== 3) return false;

  const otherPlayersCompanyMails = [];
  for(let i = 0; i < otherPlayersCompany.length; i++) {
    otherPlayersCompanyMails.push(orbiter.accounts.getEmailAddress(otherPlayersCompany[i]._id))
  }
  // const email = orbiter.accounts.getEmailAddress(playerSelected);
  const subject = 'À vos marques, prêt, feu, votez';

  const templateContent = Assets.getText('mail-template-number-of-players-reach.html');
  const templateName = 'mailTemplatePlayers';
  SSR.compileTemplate(templateName, templateContent);
  const mailHTMLContent = SSR.render(templateName, {absoluteUrl: Meteor.absoluteUrl(), newUser: newUser, appName: orbiter.core.appName});
  const SMTPSettings = Meteor.settings.smtp;
  return Email.send({
    from: SMTPSettings.from,
    bcc: otherPlayersCompanyMails,
    subject: subject,
    html: mailHTMLContent
  });
};

pollBiz.sendMailPlayersCompanyJoined = function(otherPlayersCompany, newUser) {

  const otherPlayersCompanyMails = [];
  for(let i = 0; i < otherPlayersCompany.length; i++) {
    otherPlayersCompanyMails.push(orbiter.accounts.getEmailAddress(otherPlayersCompany[i]._id))
  }

  // 4 inscrits minimum pour jouer
  const nbUsers = otherPlayersCompany.length + 1;
  const subject = 'Une nouvelle personne a rejoint Lovly';

  let templateContent = Assets.getText('mail-template-new-joiner2.html');
  if(nbUsers === 3) templateContent = Assets.getText('mail-template-new-joiner3.html');
  if(nbUsers >= 4) templateContent = Assets.getText('mail-template-new-joiner4-et-plus.html');

  const templateName = 'mailTemplatePlayers';
  SSR.compileTemplate(templateName, templateContent);
  const mailHTMLContent = SSR.render(templateName, {absoluteUrl: Meteor.absoluteUrl(), appName: orbiter.core.appName, newUser: newUser, nbUsers: nbUsers});
  const SMTPSettings = Meteor.settings.smtp;

  return Email.send({
    from: SMTPSettings.from,
    bcc: otherPlayersCompanyMails,
    subject: subject,
    html: mailHTMLContent
  });
};


pollBiz.sendMailInviteColleague = function(emails_filtered, fromUser) {
  if(!Meteor.userId())
    throw new Meteor.Error("not-logged-in", "Must be logged in");

  if(!emails_filtered || !emails_filtered.length) return;

  const subject = fromUser.first + ' ' + fromUser.last + ' has invited you to play';
  const templateContent = Assets.getText('mail-template-invite-colleague.html');
  const templateName = 'mailTemplatePlayers';
  SSR.compileTemplate(templateName, templateContent);
  const SMTPSettings = Meteor.settings.smtp;

  for(let i = 0; i < emails_filtered.length; i++) {
    const email = emails_filtered[i];
    if(email && orbiter.accounts.validateEmail(email)) {
      const mailHTMLContent = SSR.render(templateName, {absoluteUrl: Meteor.absoluteUrl(), appName: orbiter.core.appName, email: email, fromUser: fromUser});
      Email.send({
        from: SMTPSettings.from,
        to: email,
        subject: subject,
        html: mailHTMLContent
      });
    }
  }
};