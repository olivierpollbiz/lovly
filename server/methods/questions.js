Meteor.methods({

  getRandomQuestion: function(query) {
    const u = Meteor.user();
    if(!u) return false;

    query = query || {};
    return pollBiz.randomFromCollection(Questions, query);
  },

  getNextQuestion: function(question) {
    const u = Meteor.user();
    if(!u) return false;

    const n = Questions.findOne({createdAt: {$gt: question.createdAt}}, {sort: {createdAt: 1}});
    if(n) return n;
    else return Questions.findOne({}, {sort: {createdAt: 1}});

  },
});
