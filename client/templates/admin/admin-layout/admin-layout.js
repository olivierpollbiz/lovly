/**
 * Created by David FAIVRE-MAÇON on 28/11/2017.
 */

Template.adminLayout.events({

  'click .logout': function() {
    Meteor.logout();
    Router.go('/');
  }

});
