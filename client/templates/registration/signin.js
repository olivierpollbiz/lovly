/**
 * Created by David FAIVRE-MAÇON on 28/11/2017.
 */

// Meteor.loginWithLinkedIn({
//   requestPermissions: ['user_friends', 'public_profile', 'email']
// }, (err) => {
//   if (err) {
//     // handle error
//   } else {
//     // successful login!
//   }
// });


Template.signin.replaces("orbiterAccountsSignin");


Template.orbiterAccountsSignin.events({

  'click #at-facebook'(e, tpl) {
    e.preventDefault();
    e.stopPropagation();

    console.log(e);
    return
    Meteor.loginWithFacebook({
      loginStyle: "redirect",
      requestPermissions: ['public_profile', 'email']
    }, (err) => {
      if(err) {
        // handle error
        console.log(err);
      } else {
        // successful login!
        console.log('success');
      }
    });
  },

  'click #at-linkedin'(e, tpl) {
    e.preventDefault();
    e.stopPropagation();

    Meteor.loginWithLinkedIn({
      requestPermissions: ['r_basicprofile', 'r_emailaddress']
    }, (err) => {
      if(err) {
        // handle error
        console.log(err);
      } else {
        // successful login!
        console.log('success');
        // console.log(err);
        //
        // console.log(Meteor.user());
        // HTTP.call('POST',
        //   "https://api.linkedin.com/v2/connections?q=viewer&count=50", {
        //     headers : {
        //       'Content-Type':'application/x-www-form-urlencoded',
        //       'Access-Control-Allow-Origin' : '*'
        //     },
        //     params : {oauth2_access_token: "AQXE7kZnfH41Y8Oa8WNub0MJKLQNzM0hKgGwLEWrLR5oZGCP7n3zJu5EGtuDG3-D4XLpJQ4-JG7FR8MmmegGivgdbQ62uTKHRr-uyaedOZxRWYnlmbdhPH6ojAdL0HOFzZxYwBUCYYg_kJrcx09sBXX76_16sdBldbuXBk7SQjSkd0kNPGml5r09ds-YhVKpWrzPmygPwmXHU_5jf9wFuDWaJUwKsIWtXI_c7jrv4Yt0hVoPE8RipUT41zlGADN5OXFZaUH9hibqPlzWkovmzwG92lZlbX96mCVqmmGmXTEUudefJ-zJMK4WDy0DJXOYOae_BowonCNCajBv3445IuTiWa8SeA"}
        //     // params : {token: Meteor.user().services.linkedin.accessToken}
        //   },
        //   function(err,res){
        //     if(err){
        //       console.log(err);
        //     }
        //     else{
        //       console.log(res);
        //     }
        //   });

        // HTTP.get("https://api.linkedin.com/v2/connections?q=viewer&count=50", function(res) {
        //   console.log(res);
        //   //
        //   // var data = '';
        //   //
        //   // res.on('data', function (chunk){
        //   //   data += chunk;
        //   // });
        //   //
        //   // res.on('end',function(){
        //   //   // result is formatted as jsonp... this is for illustration only.
        //   //   eval(data);
        //   // })
        // })
        // https://api.linkedin.com/v2/connections?q=viewer&count=50
      }
    });
  },
});
