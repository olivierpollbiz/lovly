Meteor.methods({

  /**
   * Tags
   */

  adminTagsAdd: function(obj) {
    if(!pollBiz.isHr()) return false;
    if(!obj.name || !obj.name.trim()) return false;
    obj.name = obj.name.trim().toLowerCase();
    console.log(obj);
    obj.companyId = pollBiz.getManagedCompanyId();

    return Tags.update(obj, {$set: obj}, {upsert: true});
  },

  adminTagsUpdate: function(id, obj) {
    if(!orbiter.accounts.isAdmin()) return false;

    if(!obj.name || !obj.name.trim()) return false;
    obj.name = obj.name.trim().toLowerCase();

    const u = Meteor.user();
    obj.modifiedAt = new Date();
    obj.modifiedBy = {
      _id: u._id,
      name: orbiter.accounts.getUserFullname(u._id)
    };
    return Tags.update(id, {$set: obj});
  },

  adminTagsRemove: function(id) {
    if(!orbiter.accounts.isAdmin()) return false;

    //FIXME remove all dependencies?
    return Tags.remove(id);
  },
});