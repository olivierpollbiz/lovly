import {Meteor} from "meteor/meteor";

/**
 * Created by David FAIVRE-MAÇON on 30/11/2017.
 */

Template.pollbizAdminHrUsers.onCreated(function() {
  this.fromDate = new ReactiveVar(0);
  this.toDate = new ReactiveVar(0);

  // FIXME à changer quand multi company
  this.subscribe('users-hr-company', Meteor.userId());
});

Template.pollbizAdminHrUsers.onRendered(function() {
  const $calendar = $(".datepicker");
  $calendar.datepicker();
  $calendar.datepicker("option", $.datepicker.regional['en']);
  $calendar.datepicker("option", "dateFormat", "yy-mm-dd");
});

Template.pollbizAdminHrUsers.helpers({

  users: function() {
    // FIXME à changer quand multi company
    const c = Companies.findOne({hrId: Meteor.userId()});
    return Meteor.users.find({"profile.company._id": c._id}, {sort: {"profile.company.name": 1, "profile.obName.last": 1}});
  },

  voteReceveid: function() {

    const query = {"to._id": this._id};
    const fromDate = Template.instance().fromDate.get();
    if(fromDate) query.votedAt = {$gt: new Date(fromDate)};
    const toDate = Template.instance().toDate.get();
    if(toDate) {
      if(fromDate) query.votedAt = {$gt: new Date(fromDate), $lt: new Date(toDate)};
      else
        query.votedAt = {$lt: new Date(toDate)};
    }
    return Votes.find(query).count();
  },

  voteAnswered: function() {
    const query = {"from._id": this._id};
    const fromDate = Template.instance().fromDate.get();
    if(fromDate) query.votedAt = {$gt: new Date(fromDate)};
    const toDate = Template.instance().toDate.get();

    Session.set('postSubmitErrors', {});
    if(toDate) {
      if(fromDate) query.votedAt = {$gt: new Date(fromDate), $lt: new Date(toDate)};
      else
        query.votedAt = {$lt: new Date(toDate)};

      if(new Date(toDate).getTime() < new Date(fromDate).getTime()) return Session.set('postSubmitErrors', {dateTo: "to date must be greater than from date"});

    }

    return Votes.find(query).count();
  },

  hasCompany: function() {
    return Companies.findOne({hrId: Meteor.userId()});
  },
});


Template.pollbizAdminHrUsers.events({

  'change #dateFromInput': function(e, tpl) {
    tpl.fromDate.set($(e.currentTarget).val());
  },

  'change #dateToInput': function(e, tpl) {
    tpl.toDate.set($(e.currentTarget).val());
  },

  'click .js-hr-router-go-user': function() {
    Router.go('pollbizAdminHrUser', {_id: this._id});
  },

  'click .delete': function() {
    const that = this;
    const translation = TAPi18n.__('deleteConfirmationMessage');
    bootbox.confirm(translation, function(result) {
      if(!result) return;
      Meteor.call('pollbiz.hr.user.remove', that._id);
    });
  },

  'click .js-send-invitation'(e, tpl) {
    e.preventDefault();

    let form = $('#invit').serializeJSON();
    form.email = form.email.filter(function(n) { return n.trim !== '' });

    $('.js-send-invitation').hide();
    Meteor.call('pollBizInviteColleagueSend', form.email, function(err, res) {
      if(err)
        orbiter.core.error("error in pollBizInviteFromHrSend", ['sendMail'], form);
      else {
        orbiter.core.displayMessage("Invitation sent", "success", {class: "alert-small", autoClose: true, autoCloseTime: 2000});
        $('form')[0].reset();
        $('.js-send-invitation').show();
      }
    })
  }
});

/**
 * Edit
 */

Template.pollbizAdminHrUsersEdit.onCreated(function() {
  this.companySelected = new ReactiveVar(0);

  Session.set('postSubmitErrors', {});
});

Template.pollbizAdminHrUsersEdit.onRendered(function() {
  $("#roles").select2();
});

Template.pollbizAdminHrUsersEdit.helpers({

  editedUser: function() {
    const u = Meteor.users.findOne(Session.get('orbiterAdminUsersEdit'));
    if(u && u.profile && u.profile.company && u.profile.company._id)
      Template.instance().companySelected.set(u.profile.company._id);
    return u;
  },

  roles: function() {
    if(!orbiter.accounts.isWebmaster())
      return Meteor.roles.find({name: {$ne: 'webmaster'}}, {sort: {name: 1}});
    else
      return Roles.getAllRoles().fetch();
  },

  selected: function() {
    if(!this.parent.roles) return '';
    return this.parent.roles.indexOf(this.context.name) > -1 ? 'selected' : ''
  },

  company() {
    return {
      position: "top",
      limit: 5,
      rules: [
        {
          collection: 'Companies',
          field: "name",
          subscription: 'autocompleteCompany',
          template: Template.adminUsersCompanyAutocomplete
        },
      ]
    };
  }
});

Template.pollbizAdminHrUsersEdit.events({

  'autocompleteselect input': function(e, tpl, doc) {
    // console.log("selected ", doc);
    tpl.companySelected.set(doc._id);
  },

  'click .submit': function(e, tpl) {
    e.preventDefault();
    const form = tpl.$('form').serializeJSON();

    form['profile.company'] = {
      _id: tpl.companySelected.get(),
      name: $('#company-auto-complete').val()
    };

    // delete form['profile.company.name'];
    console.log(form);

    const that = this;
    Meteor.call('userUpdate', form, this._id, function(err) {
      if(!err) {
        Session.set('orbiterAdminUsersEdit', '');
        Meteor.call('checkSendMailToOther', that);
      }
      else orbiter.core.showException(err);
    });
  },

  'click .cancel': function(e, tpl) {
    Session.set('orbiterAdminUsersEdit', '');
  }

});
