// config for useraccounts

T9n.setLanguage('fr');

AccountsTemplates.configure({
  // Behavior
  confirmPassword: true,
  enablePasswordChange: true,
  forbidClientAccountCreation: false,
  overrideLoginErrors: true,
  sendVerificationEmail: true,
  lowercaseUsername: false,
  focusFirstInput: true,

  // Appearance
  showAddRemoveServices: true,
  showForgotPasswordLink: true,
  showLabels: true,
  showPlaceholders: true,
  showResendVerificationEmailLink: true,

  // Client-side Validation
  continuousValidation: false,
  negativeFeedback: false,
  negativeValidation: true,
  positiveValidation: true,
  positiveFeedback: true,
  showValidating: true,

  // Privacy Policy and Terms of Use
  privacyUrl: 'privacy',
  termsUrl: 'terms-of-use',

  // Redirects
  homeRoutePath: '/',
  redirectTimeout: 4000,

  // Hooks
  //onLogoutHook: myLogoutFunc,
  //onSubmitHook: mySubmitFunc,
  //preSignUpHook: myPreSubmitFunc,
  //postSignUpHook: myPostSubmitFunc,

  // Texts
  //texts: {
  //  button: {
  //    signUp: "Register Now!"
  //  },
  //  socialSignUp: "Register",
  //  socialIcons: {
  //    "meteor-developer": "fa fa-rocket"
  //  },
  //  title: {
  //    forgotPwd: "Recover Your Password"
  //  },
  //},
});

// AccountsTemplates.addFields([
//   {
//     _id: "username",
//     type: "text",
//     displayName: "username",
//     required: true,
//     minLength: 4,
//   }
// ]);

//AccountsTemplates.addField({
//  _id: "gender",
//  type: "select",
//  displayName: "Gender",
//  select: [
//    {
//      text: "Male",
//      value: "male",
//    },
//    {
//      text: "Female",
//      value: "female",
//    },
//  ],
//});
