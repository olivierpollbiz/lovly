import {Meteor} from "meteor/meteor";

/**
 * Created by David FAIVRE-MAÇON on 15 december 2017.
 */

Template.inviteColleague.onCreated(function() {
});

Template.inviteColleague.helpers({

  playerNeeded() {
    const u = Meteor.user();
    return Meteor.users.find({"profile.company._id": u.profile.company._id}).count() < 4;
  },

  nbPlayerNeedeed() {
    const u = Meteor.user();
    const playerRegistred = Meteor.users.find({"profile.company._id": u.profile.company._id}).count();
    return 4 - playerRegistred;
  },

  facebookShare() {
    const u = Meteor.user();
    return 'https://www.facebook.com/sharer/sharer.php?u=' + encodeURIComponent('https://app.lovlyapp.com/join?schoolName=' + u.profile.company.name)
  }
});

Template.inviteColleague.events({

  'click .js-add-colleague'(e, tpl) {
    $('.js-email:first').clone().insertAfter(".js-email:last");
    $('.js-email:last input').val('');
    $('.js-email:last input').focus()
  },

  // 'keyup input'(e, tpl) {
  //
  // },

  'click .btn-send-invitation'(e, tpl) {
    e.preventDefault();

    let form = $('form').serializeJSON();
    form.email = form.email.filter(function(n) { return n.trim !== '' });

    $('.btn-send-invitation').hide();
    Meteor.call('pollBizInviteColleagueSend', form.email, function(err, res) {
      if(err)
        orbiter.core.error("error in pollBizInviteColleagueSend", ['sendMail'], form);
      else {
        orbiter.core.displayMessage("Invitation envoyée", "success");

        const currentRoute = Router.current();
        const routeName = currentRoute.route.getName();
        if(routeName === "inviteColleague")
          Router.go('home');
        else {
          $('form')[0].reset();
          $('.btn-send-invitation').show();
        }
      }
    })
  }

});
