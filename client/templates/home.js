/**
 * Created by David FAIVRE-MAÇON on 28/11/2017.
 */

import {ReactiveVar} from 'meteor/reactive-var';
import {Meteor} from "meteor/meteor";

Template.home.onCreated(function() {

  this.playerSelected = new ReactiveVar(0);
  this.randomQuestion = new ReactiveVar(0);
  this.questionSent = new ReactiveVar(0);
  this.players = new ReactiveVar(0);

  const tpl = this;

  let query = {};
  // const u = Meteor.user();
  // if(u && u.profile && u.profile.company && u.profile.company._id) {
  //   query.companyId = u.profile.company._id;
  // }
  Meteor.call('getRandomQuestion', query, function(err, question) {
    if(!err) {
      tpl.randomQuestion.set(question);
    }
    else orbiter.core.error(err)
  });
});

Template.home.helpers({

  allRequiredOk() {
    // 4 inscrits minimum pour jouer
    return Meteor.users.find().count() >= 4;
  },

  votedForMe() {
    return Votes.findOne({"to._id": Meteor.userId(), seen: {$exists: false}});
  },

  nbVotes() {
    return Votes.find({"to._id": Meteor.userId(), seen: {$exists: false}}).count();
  },

  linkVotes() {
    const vote = Votes.findOne({"to._id": Meteor.userId(), seen: {$exists: false}});
    return '/vote/' + vote._id;
  },

  questionSent() {
    return Template.instance().questionSent.get();
  },

  randomQuestion() {
    return Template.instance().randomQuestion.get();
  },

  players() {
    // just for reactive
    const reactivePlayer = Template.instance().randomQuestion.get();

    const myCompanyId = Meteor.user().profile.company._id;

    // cherche les autres collegues
    const playersFromDb = Meteor.users.find({_id: {$ne: Meteor.userId()}, "profile.company._id": myCompanyId}).fetch();

    const nbPlayersFromDb = playersFromDb.length;
    //  >4 <=6 contacts => 2 choix possibles
    //  >6 <=12 contacts => 4 choix
    //  >12 => 6 choix​
    const nbPlayersToSend = nbPlayersFromDb > 12 ? 6 : nbPlayersFromDb > 6 ? 4 : 2;
    // console.log(nbPlayersFromDb, nbPlayersToSend);
    const players = [];
    for(let i = 0; i < nbPlayersToSend; i++) {
      const rnd = Math.floor(Math.random() * (nbPlayersFromDb - i));
      players.push(playersFromDb[rnd]);
      playersFromDb.splice(rnd, 1);
    }
    Template.instance().players.set(players);
    return players;
  },

  firstName() {
    return pollBiz.getFirstName(this);
  },

  lastName() {
    return pollBiz.getLastName(this);
  },

  isPlayerSelected() {
    return this._id === Template.instance().playerSelected.get() ? "selected" : "";
  },

  facebookShare() {
    const u = Meteor.user();
    return 'https://www.facebook.com/sharer/sharer.php?u=' + encodeURIComponent('https://app.lovlyapp.com/join?schoolName=' + u.profile.company.name)
  }
});

Template.home.events({

  'click .player'(e, tpl) {
    tpl.playerSelected.set(this._id);
  },

  'click .btn-next-question, click .skip-question a'(e, tpl) {
    const $btn = $('.btn-next-question');
    $btn.text('wait...');
    $btn.removeClass('.btn-next-question');

    // let query = {};
    // const u = Meteor.user();
    // if(u && u.profile && u.profile.company && u.profile.company._id) query.companyId = u.profile.company._id;
    const question = tpl.randomQuestion.get();
    Meteor.call('getNextQuestion', question, function(err, question) {
      if(!err) {
        tpl.randomQuestion.set(question);
        tpl.questionSent.set(0);
        tpl.playerSelected.set(0);
      }
      else orbiter.core.error(err)
    });
  },

  'click .btn-send'(e, tpl) {
    const playerSelected = tpl.playerSelected.get();
    // FIXME show error message
    if(!playerSelected) {
      $('.pollbiz-must-select-player').show();
      return false;
    }
    const answer = {
      player_id: playerSelected,
      question: Template.instance().randomQuestion.get(),
      players: Template.instance().players.get()
    };

    // reset template
    tpl.playerSelected.set(0);
    tpl.questionSent.set(1);

    // $('.btn-send').hide();
    // $('.skip-question').hide();
    Meteor.call('pollBizAnswerSend', answer, function(err, res) {
      if(err || !res)
        orbiter.core.error("error in sendMail", ['sendMail'], answer);
    })
  }

});
