/**
 * Created by David FAIVRE-MAÇON on 01/12/2017.
 */


Template.pollBizAnswerVote.helpers({

  votedForMe() {
    const currentRoute = Router.current().route.getName();
    const currentParams = Router.current().params;
    // console.log(currentRoute);
    // console.log(currentParams);
    if(currentRoute === "home")
      return Votes.findOne({"to._id": Meteor.userId(), seen: {$exists: false}});
    else if(currentRoute === "pollBizAnswerVote") {
      return Votes.findOne(currentParams.vote_id);
    }
  },

  players() {
    return this.players;
  },

  firstName() {
    return pollBiz.getFirstName(this);
  },

  lastName() {
    return pollBiz.getLastName(this);
  },

  isPlayerSelected(s) {
    return this._id === s.to._id ? "anwser-selected" : "player-novoted";
  },

  haveMoreAnswer() {
    return Votes.find({"to._id": Meteor.userId(), seen: {$exists: false}}).count() > 1;
  },

  linkVotes() {
    const vote_id = Router.current().params.vote_id;
    console.log(vote_id);
    const vote = Votes.findOne({"_id": {$ne: vote_id}, "to._id": Meteor.userId(), seen: {$exists: false}});
    return '/vote/' + vote._id;
  },
});

Template.pollBizAnswerVote.events({

  'click .js-set-vote-as-seen'(e, tpl) {
    const link = $(e.currentTarget).attr('href');
    const that = this;
    Meteor.call('pollBizVotesSetSeen', this._id, function(err) {
      if(err)
        orbiter.core.error("pollBizVotesSetSeen", ['votes'], that);
      else Router.go(link);
    })
  },
});