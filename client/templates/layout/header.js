Template.header.helpers({

  smallIfConnected() {
    return Meteor.userId() ? "small" : "";
  }
});