/**
 * Created by David FAIVRE-MAÇON on 28/11/2017 modified on 26/04/2018.
 */

const ERRORS_KEY = 'joinErrors';

Template.profile.onRendered(function() {
  $('#btn-profile-creating').hide();

  const mySchoolJoin = Session.get('mySchoolJoin');
  console.log(mySchoolJoin);
  if(mySchoolJoin) {
    const $auto = $('#company-auto-complete');
    $auto.val(mySchoolJoin);
    setTimeout(function() {$auto.trigger('keyup');}, 500);
  }
});

Template.profile.helpers({

  profileCompleted() {
    console.log(this);
    return pollBiz.profileCompleted();
  },

  user() {
    return Meteor.user();
  },

  company() {
    return {
      position: "top",
      limit: 5,
      rules: [
        {
          collection: 'Companies',
          field: "name",
          subscription: 'autocompleteCompany',
          template: Template.profileCompanyAutocomplete
        },
      ]
    };
  }
});

Template.profile.events({

  'keyup #company-auto-complete': function(e, tpl) {
    const company = e.currentTarget.value;
    // console.log(company);
    $('[name=companyNew]').val(company);
    $('[name=companyId]').val('');
  },

  'autocompleteselect input': function(e, tpl, doc) {
    // console.log("selected ", doc);
    $('[name=companyId]').val(doc._id);
    $('[name=companyNew]').val('');
  },

  'click .submit, submit': function(e, tpl) {
    e.preventDefault();

    let u = tpl.$('form').serializeJSON();

    // console.log(u);
    let errors = {};
    // if(!u.first) errors.first = "First Name Required";
    // if(!u.last) errors.last = "Last Name Required";
    if(!u.companyNew && !u.companyId) errors.company = "Company Required";

    if(Object.keys(errors).length) return Session.set('postSubmitErrors', errors);
    else Session.set('postSubmitErrors', {});

    $('#btn-profile-submit').hide();
    $('#btn-profile-creating').show();
    Meteor.call('profileUpdate', u, function(err) {
      if(err) return Session.set(ERRORS_KEY, {'none': err.reason});
      else Router.go('home');
    });
  }

});