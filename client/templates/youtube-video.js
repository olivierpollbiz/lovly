Template.youtubeVideo.onRendered(function() {

  // YouTube API will call onYouTubeIframeAPIReady() when API ready.
  // Make sure it's a global variable.

  // console.log(this);

  const that = this;
  let player;

  let videoId = Template.currentData().videoId;
  if(videoId) {
    onYouTubeIframeAPIReady = function() {
      // New Video Player, the first argument is the id of the div.
      // Make sure it's a global variable.
      player = new YT.Player("youtube-video", {
        height: "100%",
        width: "100%",
        // videoId is the "v" in URL (ex: http://www.youtube.com/watch?v=LdH1hSWGFGU, videoId = "LdH1hSWGFGU")
        videoId: videoId,
        host: 'https://www.youtube.com',
        // Events like ready, state change,
        events: {
          onReady: function(e) {
            // Play video when player ready.
            // e.target.playVideo();
            console.log(videoId);
            e.target.loadVideoById(videoId);
            e.target.playVideo();
          }
        }
      });

    };
    YT.load();
  }

  that.autorun(function() {
    videoId = Template.currentData().videoId;
    if(player) {
      player.loadVideoById(videoId);
      player.playVideo();
    }
  });

});

Template.youtubeVideo.helpers({});