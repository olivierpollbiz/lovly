/**
 * Edit
 */

Template.pollbizAdminHrQuestionEdit.onRendered(function() {

  // console.log(this.data);
  const tpl = this.data;
  // Tags
  // FIXME subscribe security
  let allTags = Tags.find({type: "question", companyId: tpl.companyId}).fetch();
  // console.log(allTags);
  const $allTags = $("#tagsInput");

  const tags = new Bloodhound({
    datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    local: allTags
  });
  tags.initialize();

  $allTags.tagsinput({
    typeaheadjs: {
      name: 'question',
      displayKey: 'name',
      valueKey: 'name',
      source: tags.ttAdapter()
    },
  });

  $allTags.change(function() {
    const tags = $(this).tagsinput('items');
    // console.log(tags);
    _.each(tags, function(tag) {
      const docId = Tags.findOne({type: "question", name: tag.toLowerCase(), companyId: tpl.companyId});
      if(!docId) {
        Meteor.call('adminTagsAdd', {type: "question", name: tag.toLowerCase()}, function(err) {
          if(!err) allTags = Tags.find({type: "question"}).fetch();
          else throw new Meteor.Error("TagsInsert", err);
        });
      }
    });

  });

  if(this.data.tags && this.data.tags.length) $allTags.tagsinput('add', this.data.tags.join(','));
});

Template.pollbizAdminHrQuestionEdit.events({

  'submit': function(e, tpl) {
    e.preventDefault();

    let form = $('form').serializeJSON();

    let errors = {};
    if(!form.question) errors.question = "Question Required";

    if(Object.keys(errors).length) return Session.set('postSubmitErrors', errors);
    else Session.set('postSubmitErrors', {});

    if(!form.tags) form.tags = '';
    else form.tags = form.tags.toLowerCase().split(',');

    Meteor.call('hrQuestionsUpdate', this._id, form, function(err) {
      if(!err) {
        Router.go('pollbizAdminHrQuestions');
      }
    });
  }
});