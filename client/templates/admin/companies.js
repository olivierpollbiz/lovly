/**
 * Created by David FAIVRE-MAÇON on 28/11/2017.
 */

Template.pollbizAdminCompanies.onCreated(function() {

  this.companyEditId = new ReactiveVar(0);
  this.filterName = new ReactiveVar('');

  Session.set('limit', 20);

  const that = this;
  that.subscribe('pollbiz-admin-companies', {}, {limit: 20});

  that.autorun(function() {
    const filterName = Template.instance().filterName.get();
    const limit = Session.get('limit');
    that.subscribe('pollbiz-admin-companies', filterName, {limit: limit})
  });
});

Template.pollbizAdminCompanies.onRendered(function() {
  $('#wait').hide();
});

Template.pollbizAdminCompanies.helpers({

  companies() {
    const filterName = Template.instance().filterName.get();
    const regex = new RegExp(filterName, 'i');
    const options = options || {};
    options.sort = {name: 1};
    options.limit = Session.get('limit') || 20;

    return Companies.find({name: {$regex: regex}}, options);
  },

  isEditing: function() {
    return Template.instance().companyEditId.get();
  },

  hr: function() {
    if(!this.hrId) return false;
    const hr = Meteor.users.findOne(this.hrId);
    console.log(this);
    console.log(hr);
    return hr;
  },
});

Template.pollbizAdminCompanies.events({

  'keyup [name=company-filter]': function(e, tpl) {
    tpl.filterName.set(e.currentTarget.value);
  },

  'click .edit': function(e, tpl) {
    tpl.companyEditId.set(this._id);
  },

  'click .delete': function() {
    const that = this;
    const translation = TAPi18n.__('deleteConfirmationMessage');
    bootbox.confirm(translation, function(result) {
      if(!result) return;
      Meteor.call('adminCompanyRemove', that._id);
    });
  },

  'submit #import': function(e, tpl) {
    e.preventDefault();

    const $city = $('input[name=city]');
    const $lines = $('textarea[name=import]');
    const city = $city.val();
    let lines = $lines.val();

    let errors = {};
    if(!city) errors.city = "city Required";
    if(!lines) errors.import = "import Required";

    if(Object.keys(errors).length) return Session.set('postSubmitErrors', errors);
    else Session.set('postSubmitErrors', {});

    $('#submit').hide();
    $('#wait').show();

    const u = Meteor.user();
    lines = lines.split('\n');
    const data = [];
    for(let i = 0; i < lines.length; i++) {
      if(lines[i].trim()) {
        data.push({
          name: lines[i],
          city: city,
          createdAt: new Date(),
          createdBy: {
            _id: u._id,
            name: pollBiz.getFullName(u)
          },
        });
      }
    }

    Meteor.call('adminCompaniesImport', data, function(err) {
      if(!err) {
        $city.val('');
        $lines.val('');
        $('#submit').show();
        $('#wait').hide();
      }
    });
  }
});


/**
 * Edit
 */

Template.pollbizAdminCompanyEdit.onCreated(function() {
  this.companyEditId = Blaze.currentView.parentView.parentView.templateInstance().companyEditId;
  Session.set('postSubmitErrors', {});

});

Template.pollbizAdminCompanyEdit.helpers({

  editedCompany() {
    return Companies.findOne(Template.instance().companyEditId.get());
  },
});


Template.pollbizAdminCompanyEdit.events({

  'click .cancel': function(e, tpl) {
    tpl.companyEditId.set(0);
  },

  'submit #edit': function(e, tpl) {
    e.preventDefault();

    let c = tpl.$('form').serializeJSON();
    let errors = {};
    if(!c.name) errors.name = "Name Required";

    if(Object.keys(errors).length) return Session.set('postSubmitErrors', errors);
    else Session.set('postSubmitErrors', {});

    Meteor.call('adminCompanyUpdate', this._id, c, function(err) {
      if(!err) {
        tpl.companyEditId.set(0)
      }
    });
  }
});