/**
 * Created by David FAIVRE-MAÇON on 28/11/2017.
 */

Questions = new Mongo.Collection('pollbiz_questions');
Companies = new Mongo.Collection('pollbiz_companies');
Votes = new Mongo.Collection('pollbiz_votes');
Tags = new Mongo.Collection('pollbiz_tags');
