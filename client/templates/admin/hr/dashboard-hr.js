/**
 * Created by David FAIVRE-MAÇON on 30/11/2017.
 */

Template.pollbizAdminHrDashboard.onCreated(function() {
  Meteor.call('getCompaniesCount', 'hr', function(err, companiesCount,) {
    if(!err)
      Session.set('companiesCount', companiesCount);
  });

  Meteor.call('getQuestionsCount', 'hr', function(err, questionsCount,) {
    if(!err)
      Session.set('questionsCount', questionsCount);
  });

  Meteor.call('getUsersCount', 'hr', function(err, usersCount,) {
    if(!err)
      Session.set('usersCount', usersCount);
  });

  Meteor.call('getVotesCount', 'hr', function(err, votesCount,) {
    if(!err)
      Session.set('votesCount', votesCount);
  });
});

Template.pollbizAdminHrDashboard.helpers({

  nbCompanies() {
    return Session.get('companiesCount')
  },

  nbQuestions() {
    return Session.get('questionsCount')
  },

  nbUsers() {
    return Session.get('usersCount')
  },

  nbVotes() {
    return Session.get('votesCount')
  },
});

Template.pollbizAdminHrDashboard.events({

  'submit #import': function(e, tpl) {
    e.preventDefault();
    const lines = $('textarea[name=import]').val().split('\n');
    Meteor.call('adminQuestionsImport', lines, function(err) {
      if(!err) {
        $('textarea[name=import]').val('')
      }
    });
  }
});