/**
 * Created by David FAIVRE-MAÇON on 30/11/2017.
 */

Template.pollbizAdminDashboard.onCreated(function() {
  Meteor.call('getCompaniesCount', false, function(err, companiesCount,) {
    if(!err)
      Session.set('companiesCount', companiesCount);
  });

  Meteor.call('getQuestionsCount', false, function(err, questionsCount,) {
    if(!err)
      Session.set('questionsCount', questionsCount);
  });

  Meteor.call('getUsersCount', false, function(err, usersCount,) {
    if(!err)
      Session.set('usersCount', usersCount);
  });

  Meteor.call('getVotesCount', false, function(err, votesCount,) {
    if(!err)
      Session.set('votesCount', votesCount);
  });
});

Template.pollbizAdminDashboard.helpers({

  nbCompanies() {
    return Session.get('companiesCount')
  },

  nbQuestions() {
    return Session.get('questionsCount')
  },

  nbUsers() {
    return Session.get('usersCount')
  },

  nbVotes() {
    return Session.get('votesCount')
  },
});

Template.pollbizAdminDashboard.events({

  'submit #import': function(e, tpl) {
    e.preventDefault();
    const lines = $('textarea[name=import]').val().split('\n');
    Meteor.call('adminQuestionsImport', lines, function(err) {
      if(!err) {
        $('textarea[name=import]').val('')
      }
    });
  }
});