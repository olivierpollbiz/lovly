/**
 * Created by David FAIVRE-MAÇON on 28/11/2017.
 */

const ERRORS_KEY = 'joinErrors';

Template.join.replaces("orbiterAccountsJoin");

Template.orbiterAccountsJoin.onCreated(function() {
  Session.set('postSubmitErrors', {});
});

Template.orbiterAccountsJoin.onRendered(function() {
  let currentRoute = Router.current();
  const query = currentRoute.params.query;
  if(query && query.schoolName) Session.set('mySchoolJoin', query.schoolName);
  console.log(query);
  // Session.set('joinQuery', query);
  // if(query.email) $('[name=email]').val(query.email);
});

Template.orbiterAccountsJoin.events({

  'click #at-facebook'(e, tpl) {
    e.preventDefault();
    e.stopPropagation();

    Meteor.loginWithFacebook({
      loginStyle: "redirect",
      requestPermissions: ['public_profile', 'email']
    }, (err) => {
      if(err) {
        // handle error
        console.log(err);
      } else {
        // successful login!
        console.log('success');
      }
    });
  },

  'click #at-linkedin'(e, tpl) {
    e.preventDefault();
    e.stopPropagation();

    Meteor.loginWithLinkedIn({
      requestPermissions: ['r_basicprofile', 'r_emailaddress']
    }, (err) => {
      if(err) {
        // handle error
        console.log(err);
      } else {
        // successful login!
        console.log('success');
      }
    });
  },

  'submit #join': function(e, tpl) {
    e.preventDefault();

    let u = tpl.$('#join').serializeJSON();

    console.log(u);
    let errors = {};
    if(!u.email) errors.email = "emailRequired";
    if(!u.password) errors.password = "passwordRequired";
    if(u.password !== u.confirm) errors.confirm = "confirmRequired";

    if(Object.keys(errors).length) return Session.set('postSubmitErrors', errors);
    else Session.set('postSubmitErrors', {});

    delete u['confirm'];

    Meteor.call('orbiterAccountsCreate', u, function(err) {
      if(err) return Session.set(ERRORS_KEY, {'none': err.reason});
      Meteor.loginWithPassword(u.email, u.password);
    });
  }

});
