Meteor.methods({

  adminQuestionsImport: function(lines) {
    const u = Meteor.user();
    if(!u || !Roles.userIsInRole(u, ['webmaster', 'admin'])) return false;

    for(let i = 0; i < lines.length; i++) {
      if(lines[i].trim()) {
        Questions.insert({
          question: lines[i],
          createdAt: new Date(),
          createdBy: {
            _id: u._id,
            name: pollBiz.getFullName(u)
          },
        });
      }
    }
  },

  adminQuestionsUpdate: function(id, form) {
    const u = Meteor.user();
    if(!u || !Roles.userIsInRole(u, ['webmaster', 'admin'])) return false;

    form.modifiedAt = new Date();
    form.modifiedBy = {_id: u._id, name: pollBiz.getFullName(u)};
    Questions.update(id, {$set: form});
  },

  adminQuestionsRemove: function(id) {
    const u = Meteor.user();
    if(!u || !Roles.userIsInRole(u, ['webmaster', 'admin'])) return false;

    return Questions.remove(id);
  },

  adminCompaniesImport: function(data) {
    const u = Meteor.user();
    if(!u || !Roles.userIsInRole(u, ['webmaster', 'admin'])) return false;

    return Companies.batchInsert(data);
  },

  adminCompaniesHrAdd: function(company, hrId) {
    if(!pollBiz.isHr()) return false;

    let hr = Meteor.user();
    if(hrId) hr = Meteor.users.findOne(hrId);

    //FIXME en attendant la gestion multicompanie, je suprime les companies gérées
    // if(!company.companyNew && !company.companyId) {
    // return
    Companies.update({hrId: hr._id}, {$unset: {hrId: 1}});
    // }

    let profileCompany = {};
    if(company.companyId) {
      const c = Companies.findOne(company.companyId);
      if(c) {
        if(c.hrId) {
          if(c.hrId === hr._id)
            orbiter.core.error('it\'s already your company', ['companyHr']);
          else {
            const oldHr = Meteor.users.findOne(c.hrId);
            if(!oldHr || !pollBiz.isHr(c.hrId)) {
              // si l'utilisateur n'existe plus ou n'est plus Hr, je recupere la company
              Companies.update(c._id, {$set: {hrId: hr._id}});
            }
            else {
              orbiter.core.error('this company already has a hr, companyId', ['companyHr']);
              orbiter.core.displayMessage('this company already has a hr');
            }
          }
        }
        else {
          Companies.update(c._id, {$set: {hrId: hr._id}});
        }
      }
      else
        orbiter.core.error('Company not found', ['companyHr']);
    }
    else if(company.companyNew) {
      const c = Companies.findOne({name: {"$regex": "^" + company.companyNew + "\\b", "$options": "i"}});
      if(c) {
        if(c.hrId) {
          if(c.hrId === hr._id)
            orbiter.core.error('it\'s already your company', ['companyHr']);
          else
            orbiter.core.error('this company already has a hr, companyNew', ['companyHr']);
        }
        else {
          Companies.update(c._id, {$set: {hrId: hr._id}});
        }
      }
      else {
        const newCompanyId = Companies.insert({
          name: company.companyNew,
          hrId: hr._id,
          createdAt: new Date(),
          createdBy: {
            _id: hr._id,
            name: pollBiz.getFullName(hr)
          },
        });
      }
    }
    else return orbiter.core.error('Company name error', ['profile']);
  },

  adminCompanyUpdate: function(id, company) {
    const u = Meteor.user();
    if(!u || !Roles.userIsInRole(u, ['webmaster', 'admin'])) return false;

    // Mets à jour le nom des companies d'utilisateurs
    Meteor.users.update({"profile.company._id": id},{$set: {"profile.company.name": company.name}});

    if(company.name.trim() === "") return;
    Companies.update(id, {
      $set: {
        name: company.name,
        city: company.city,
        modifiedAt: new Date(),
        modifiedBy: {
          _id: u._id,
          name: pollBiz.getFullName(u)
        }
      },
    });
  },

  adminCompanyRemove: function(id) {
    const u = Meteor.user();
    if(!u || !Roles.userIsInRole(u, ['webmaster', 'admin'])) return false;

    return Companies.remove(id);
  },

  getCompaniesCount: function(hr) {
    if(!hr && orbiter.accounts.isAdmin())
      return Companies.find().count();
    if(pollBiz.isHr()) {
      const c = Companies.find({hrId: Meteor.userId()});
      return c.count();
    }
  },

  getQuestionsCount: function(hr) {
    if(!hr && orbiter.accounts.isAdmin())
      return Questions.find({"companyId": {$exists: false}}).count();
    if(pollBiz.isHr()) {
      const company = Companies.findOne({hrId: Meteor.userId()});
      if(!company) return 0;
      //FIXME et si j'ai plusieurs company?
      return Questions.find({"companyId": company._id}).count();
    }
  },

  getUsersCount: function(hr) {
    if(!hr && orbiter.accounts.isAdmin())
      return Meteor.users.find().count();
    if(pollBiz.isHr()) {
      const company = Companies.findOne({hrId: Meteor.userId()});
      if(!company) return 0;
      //FIXME et si j'ai plusieurs company?
      return Meteor.users.find({"profile.company._id": company._id}).count();
    }
  },

  getVotesCount: function(hr) {
    if(!hr && orbiter.accounts.isAdmin())
      return Votes.find().count();
    if(pollBiz.isHr()) {
      const company = Companies.findOne({hrId: Meteor.userId()});
      if(!company) return 0;
      //FIXME et si j'ai plusieurs company?
      return Votes.find({"companyId": company._id}).count();
    }
  },
});
