Meteor.methods({

  hrQuestionsInsert: function(lines) {
    if(!pollBiz.isHr()) return false;

    const u = Meteor.user();
    console.log(u);
    const companyId = pollBiz.getManagedCompanyId(u);
    console.log(companyId);
    if(!companyId) return false;
    for(let i = 0; i < lines.length; i++) {
      if(lines[i].trim()) {
        const lineArray = lines[i].split('\t');
        console.log(lineArray);
        const question = lineArray[0];
        lineArray.shift();
        Questions.insert({
          question: question,
          createdAt: new Date(),
          createdBy: {
            _id: u._id,
            name: pollBiz.getFullName(u)
          },
          tags: lineArray,
          companyId: companyId
        });
      }
    }
  },

  hrQuestionsUpdate: function(id, form) {
    if(!pollBiz.isHr()) return false;

    const u = Meteor.user();
    Questions.update(id, {
      $set: {
        question: form.question,
        videoId: form.videoId,
        pictureId: form.pictureId,
        tags: form.tags,
        modifiedAt: new Date(),
        modifiedBy: {
          _id: u._id,
          name: pollBiz.getFullName(u)
        }
      },
    });
  },

  hrQuestionsRemove: function(id) {
    if(!pollBiz.isHr()) return false;
    const u = Meteor.user();
    const companyId = pollBiz.getManagedCompanyId(u);
    const q = Questions.findOne(id);
    if(q && q.companyId === companyId)
      return Questions.remove(id);
  },

  'pollbiz.hr.user.remove': function(userId) {
    if(!pollBiz.isHr()) return orbiter.core.error('userRemove called but user is not hr', ['security', 'error']);

    const user = Meteor.users.findOne(userId);
    if(!user.profile.company || user.profile.company._id !== Meteor.user().profile.company._id) return orbiter.core.error('userRemove called but user is not from my company', ['security', 'error']);

    return Meteor.users.remove({_id: userId});
  },
});