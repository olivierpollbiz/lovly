/**
 * Created by David FAIVRE-MAÇON on 30/11/2017.
 */

import {ReactiveVar} from 'meteor/reactive-var';
import {Meteor} from "meteor/meteor";

Template.pollbizAdminUsers.replaces("orbiterAdminUsers");

Template.orbiterAdminUsers.onCreated(function() {

  this.subscribe('users');
  this.subscribe('roles');
  // this.subscribe('users-hr-votes');

  Session.set('orbiterAdminUsersEdit', '');
});

Template.orbiterAdminUsers.helpers({

  users: function() {
    return Meteor.users.find({}, {sort: {"profile.company.name": 1, "profile.obName.last": 1, "profile.obName.first": 1}});
  },
});

Template.pollbizAdminUsersOne.onCreated(function() {
  this.subscribe('admin-votes-user', this.data._id);
  this.subscribe('users-hr-company', this.data._id);
});

Template.pollbizAdminUsersOne.helpers({

  voteReceveid: function() {
    return Votes.find({"to._id": this._id}).count();
  },

  voteAnswered: function() {
    return Votes.find({"from._id": this._id}).count();
  },

  managedCompany: function() {
    return Companies.find({hrId: this._id});
  },

  canEdit: function() {
    return orbiter.accounts.isAdmin();
  },

  canDelete: function() {
    return orbiter.accounts.isAdmin();
  },
});

/**
 * Edit
 */

Template.pollbizAdminUsersEdit.onCreated(function() {
  this.companySelected = new ReactiveVar(0);
  this.managedCompanySelected = new ReactiveVar(0);

  Session.set('postSubmitErrors', {});

  console.log(Session.get('orbiterAdminUsersEdit'));
  this.subscribe('users-hr-company', Session.get('orbiterAdminUsersEdit'));
});

Template.pollbizAdminUsersEdit.onRendered(function() {
  $("#roles").select2();
});

Template.pollbizAdminUsersEdit.helpers({

  editedUser: function() {
    const u = Meteor.users.findOne(Session.get('orbiterAdminUsersEdit'));
    if(u && u.profile && u.profile.company && u.profile.company._id)
      Template.instance().companySelected.set(u.profile.company._id);
    return u;
  },

  roles: function() {
    if(!orbiter.accounts.isWebmaster())
      return Meteor.roles.find({name: {$ne: 'webmaster'}}, {sort: {name: 1}});
    else
      return Roles.getAllRoles().fetch();
  },

  selected: function() {
    if(!this.parent.roles) return '';
    return this.parent.roles.indexOf(this.context.name) > -1 ? 'selected' : ''
  },

  company() {
    return {
      position: "top",
      limit: 5,
      rules: [
        {
          collection: 'Companies',
          field: "name",
          subscription: 'autocompleteCompany',
          template: Template.adminUsersCompanyAutocomplete
        },
      ]
    };
  },

  managedCompany() {
    return {
      position: "top",
      limit: 5,
      rules: [
        {
          collection: 'Companies',
          field: "name",
          subscription: 'autocompleteCompany',
          template: Template.adminUsersCompanyAutocomplete
        },
      ]
    };
  },

  managedCompanyValue() {
    // console.log(this);
    const c = Companies.findOne({hrId: this._id});
    // console.log(c);
    if(c) return c.name;
  }
});

Template.pollbizAdminUsersEdit.events({

  'autocompleteselect #company-auto-complete': function(e, tpl, doc) {
    console.log("selected ", doc);
    tpl.companySelected.set(doc._id);
  },

  'autocompleteselect #managed-company-auto-complete': function(e, tpl, doc) {
    console.log("selected ", doc);
    tpl.managedCompanySelected.set(doc._id);
  },

  'click .submit': function(e, tpl) {
    e.preventDefault();
    const form = tpl.$('form').serializeJSON();

    form['profile.company'] = {
      _id: tpl.companySelected.get(),
      name: $('#company-auto-complete').val()
    };

    // delete form['profile.company.name'];
    // console.log(form);
    if(!form.roles) form.roles = [];


    // update managed company

    const formCompany = {
      companyNew: $('#managed-company-auto-complete').val(),
      companyId: tpl.managedCompanySelected.get()
    };

    console.log(formCompany);
    Meteor.call('adminCompaniesHrAdd', formCompany, this._id, function(err) {
      if(!err) {
      }
      else
        orbiter.core.error(err)
    });

    const that = this;
    Meteor.call('userUpdate', form, this._id, function(err) {
      if(!err) {
        Session.set('orbiterAdminUsersEdit', '');
        Meteor.call('checkSendMailToOther', that);
      }
      else orbiter.core.showException(err);
    });
  },

  'click .cancel': function(e, tpl) {
    Session.set('orbiterAdminUsersEdit', '');
  }

});
