/**
 * Created by David FAIVRE-MAÇON on 28/11/2017.
 */

Router.configure({
  layoutTemplate: 'appLayout',
  notFoundTemplate: 'appNotFound',
  loadingTemplate: 'appLoading',
  title: Meteor.settings.public.appTitle || 'Lovly',
});

Router.onBeforeAction(function() {
  if(Meteor.isClient) {
    TAPi18n.setLanguage('fr');
    T9n.setLanguage("fr");
  }
  // try {
  // const lng = this.params.lng;
  // if(!lng) {
  //   const url = Router.current().url;
  //   if(url !== '/' && url.substr(0, 6) !== '/admin')
  //     orbiter.core.error('no language defined for ' + url, ['error', 'language'], null, null, {noSlack: true});
  //   return this.next();
  // }
  //   const cur = TAPi18n.getLanguage();
  //   const languages = OrbiterLanguages.find().map(l => l.code);
  //   if(cur !== lng && languages.includes(lng)) {
  //     TAPi18n.setLanguage(lng);
  //     let lngT9n = OrbiterLanguages.findOne({code: lng}).codeT9n;
  //     if(_.isEmpty(lngT9n)) lngT9n = lng;
  //     T9n.setLanguage(lngT9n);
  //   }
  // }
  // catch(e) {
  //   orbiter.core.error(e.message, ['exception'], e);
  // }
  this.next();
});


Router.route('/join', {
  name: 'join',
  template: 'orbiterAccountsJoin',
  onBeforeAction: function() {
    if(Meteor.userId()) Router.go('home');
    else this.next();
  }
});

Router.route('/signin', {
  name: 'signin',
  template: 'orbiterAccountsSignin',
  onBeforeAction: function() {
    if(Meteor.userId()) Router.go('home');
    else this.next();
  }
});

Router.route('/profile', {
  name: 'profile',
  onBeforeAction: function() {
    if(!Meteor.userId()) Router.go('join');
    if(pollBiz.profileCompleted()) Router.go('home');
    else this.next();
  }
});

Router.route('/', {
  name: 'home',
  onBeforeAction: function() {
    if(!Meteor.userId()) Router.go('join');
    else if(!pollBiz.profileCompleted()) Router.go('profile');
    else this.next();
  },
  waitOn: function() {
    return [
      Meteor.subscribe('players'),
      Meteor.subscribe('votesForMe')
    ];
  },
});

Router.route('/invite', {
  name: 'inviteColleague',
  onBeforeAction: function() {
    if(!Meteor.userId()) Router.go('join');
    else if(!pollBiz.profileCompleted()) Router.go('profile');
    else this.next();
  },
  waitOn: function() {
    return [
      Meteor.subscribe('players')
    ];
  },
});

Router.route('/vote/:vote_id', {
  name: 'pollBizAnswerVote',
  onBeforeAction: function() {
    if(!Meteor.userId()) Router.go('signin');
    else this.next();
  },
  waitOn: function() {
    return [
      Meteor.subscribe('votesForMe')
    ];
  },
});

Router.route('/link/messenger/:ecole', {
  name: 'messengerLink',
  where: "server",
  action: function() {

    const templateContent = Assets.getText('link-messenger.html');
    const templateName = 'link-messenger';
    SSR.compileTemplate(templateName, templateContent);
    const html = SSR.render(templateName, {category: 'meteor'});
    const response = this.response;

    response.writeHead(200, {'Content-Type': 'text/html'});
    response.end(html);
  }
});

// Tuto
Router.route('/tuto-lovly.html', {
  layoutTemplate: 'tutoLayout',
  name: 'tuto1'
});

Router.route('/tuto-lovly2.html', {
  layoutTemplate: 'tutoLayout',
  name: 'tuto2'
});

Router.route('/tuto-lovly3.html', {
  layoutTemplate: 'tutoLayout',
  name: 'tuto3'
});

Router.route('/tuto-lovly4.html', {
  layoutTemplate: 'tutoLayout',
  name: 'tuto4'
});

// ADMIN

Router.route('/admin', {
  name: 'admin',
  template: 'pollbizAdminDashboard',
  layoutTemplate: 'adminLayout',
  title: 'PollBiz - Admin - Dashboard',
  onBeforeAction: function() {
    if(!pollBiz.isHr())
      Router.go('/');
    else
      this.next();
  }
});

Router.route('/admin/companies', {
  name: 'pollbizAdminCompanies',
  layoutTemplate: 'adminLayout',
  title: 'PollBiz - Admin - Companies',
  onBeforeAction: function() {
    if(!pollBiz.isHr())
      Router.go('/');
    else
      this.next();
  },
  waitOn: function() {
    return [
      Meteor.subscribe('users')
    ];
  },
});

Router.route('/admin/questions', {
  name: 'pollbizAdminQuestions',
  layoutTemplate: 'adminLayout',
  title: 'PollBiz - Admin - Questions',
  onBeforeAction: function() {
    if(!orbiter.accounts.isAdmin())
      Router.go('/');
    else
      this.next();
  },
  waitOn: function() {
    return [
      Meteor.subscribe('allQuestions')
    ];
  },
});

Router.route('/admin/todo', {
  name: 'pollbizAdminTodo',
  layoutTemplate: 'adminLayout',
  title: 'Admin - Todo',
  onBeforeAction: function() {
    if(!orbiter.accounts.isAdmin())
      Router.go('/');
    else
      this.next();
  }
});

Router.route('/admin/users', {
  name: 'pollbizAdminUsers',
  template: 'orbiterAdminUsers',
  layoutTemplate: 'adminLayout',
  title: 'PollBiz - Admin - Users',
  onBeforeAction: function() {
    if(!orbiter.accounts.isAdmin())
      Router.go('/');
    else
      this.next();
  }
});

// hr

Router.route('/admin/hr', {
  name: 'adminHr',
  template: 'pollbizAdminHrDashboard',
  layoutTemplate: 'adminLayout',
  title: 'PollBiz - Admin - Hr - Dashboard',
  onBeforeAction: function() {
    if(!pollBiz.isHr())
      Router.go('/');
    else
      this.next();
  }
});

Router.route('/admin/hr/companies', {
  name: 'pollbizAdminHrCompanies',
  layoutTemplate: 'adminLayout',
  title: 'PollBiz - Admin - Hr - Companies',
  onBeforeAction: function() {
    if(!pollBiz.isHr())
      Router.go('/');
    else
      this.next();
  },
  waitOn: function() {
    return [
      Meteor.subscribe('pollbiz-admin-hr-companies')
    ];
  },
});

Router.route('/admin/hr/questions', {
  name: 'pollbizAdminHrQuestions',
  layoutTemplate: 'adminLayout',
  title: 'PollBiz - Admin - hr - Questions',
  onBeforeAction: function() {
    if(!pollBiz.isHr())
      Router.go('/');
    else
      this.next();
  },
  waitOn: function() {
    return [
      Meteor.subscribe('pollbiz-admin-hr-companies'),
      Meteor.subscribe('pollbiz-admin-hr-questions')
    ];
  },
});

Router.route('/admin/hr/questions/:_id', {
  name: 'pollbizAdminHrQuestionEdit',
  layoutTemplate: 'adminLayout',
  title: 'PollBiz - Admin - hr - Questions - edit',
  onBeforeAction: function() {
    if(!pollBiz.isHr())
      Router.go('/');
    else
      this.next();
  },
  waitOn: function() {
    return [
      Meteor.subscribe('pollbiz-admin-hr-companies'),
      Meteor.subscribe('pollbiz-admin-hr-questions'),
      Meteor.subscribe('allTagsMyManagedCompany')
    ];
  },
  data: function() {
    if(!this.ready()) return;
    const e = Questions.findOne({"_id": this.params._id});
    if(!e) this.render('appNotFound');
    else return e;
  },
});

Router.route('/admin/hr/users', {
  name: 'pollbizAdminHrUsers',
  layoutTemplate: 'adminLayout',
  title: 'PollBiz - Admin - Hr - Users',
  onBeforeAction: function() {
    if(!pollBiz.isHr())
      Router.go('/');
    else
      this.next();
  },
  waitOn: function() {
    return [
      Meteor.subscribe('pollbiz-admin-hr-companies'),
      Meteor.subscribe('users-hr'),
      Meteor.subscribe('roles'),
      Meteor.subscribe('users-hr-votes')
    ];
  },
});

Router.route('/admin/hr/user/:_id', {
  name: 'pollbizAdminHrUser',
  layoutTemplate: 'adminLayout',
  title: 'PollBiz - Admin - Hr - User',
  onBeforeAction: function() {
    if(!pollBiz.isHr())
      Router.go('/');
    else
      this.next();
  },
  waitOn: function() {
    return [
      Meteor.subscribe('pollbiz-admin-hr-companies'),
      Meteor.subscribe('users-hr'),
      Meteor.subscribe('roles'),
      Meteor.subscribe('users-hr-votes')
    ];
  },
  data: function() {
    if(!this.ready()) return;
    const e = Meteor.users.findOne({"_id": this.params._id});
    if(!e) this.render('appNotFound');
    else return e;
  },
});