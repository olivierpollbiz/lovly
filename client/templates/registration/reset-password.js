/**
 * Created by David FAIVRE-MAÇON on 28/11/2017.
 */

Template.resetPassword.replaces("orbiterAccountsReset");

const ERRORS_KEY = 'resetPasswordErrors';
const RESET_PASSWORD = 'resetPassword';
const SUCCESS_KEY = 'resetPasswordSuccessMessage';

Template.orbiterAccountsReset.events({

  'click #submit-reset-password': function(event, tpl) {
    event.preventDefault();

    const password = tpl.$('[name=password]').val();
    const confirm = tpl.$('[name=confirm]').val();

    const errors = {};
    const checkPassword = orbiter.core.checkPassword(password);

    if(!password) errors.password = TAPi18n.__('obtAccountsEmailRequired');
    if(!confirm) errors.confirm = TAPi18n.__('orbiterAccountsPasswordConfirmationRequiredErr');
    if(password !== confirm) errors.match = TAPi18n.__('orbiterAccountsPasswordDoesntMatchErr');
    Session.set(ERRORS_KEY, errors);
    if(_.keys(errors).length) return;
    if(checkPassword.length) {
      return Session.set('checkPassword', checkPassword);
    }

    Accounts.resetPassword(Session.get(RESET_PASSWORD), password, function(err) {
      if(err) {
        orbiter.core.showException(err);
      } else {
        Session.set(RESET_PASSWORD, null);
        Session.set(SUCCESS_KEY, TAPi18n.__('orbiterAccountsResetPasswordSuccessMsg'));
        Router.go('signin')
      }
    });

  },
});