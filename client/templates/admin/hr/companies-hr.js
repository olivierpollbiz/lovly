import {Meteor} from "meteor/meteor";

/**
 * Created by David FAIVRE-MAÇON on 28/11/2017.
 */

Template.pollbizAdminHrCompanies.onCreated(function() {
  this.companyEditId = new ReactiveVar(0);
  this.filterName = new ReactiveVar(0);

  Session.set('limit', 20);
});

Template.pollbizAdminHrCompanies.onRendered(function() {
  $('#wait').hide();
});

Template.pollbizAdminHrCompanies.helpers({

  hasCompany: function() {
    return Companies.findOne({hrId: Meteor.userId()});
  },

  companies() {
    return Companies.find();
  },

  company() {
    return {
      position: "bottom",
      limit: 5,
      rules: [
        {
          collection: 'Companies',
          field: "name",
          subscription: 'autocompleteCompany',
          template: Template.profileCompanyAutocomplete
        },
      ]
    };
  },

  isEditing: function() {
    return Template.instance().companyEditId.get();
  },
});

Template.pollbizAdminHrCompanies.events({

  'keyup #company-auto-complete': function(e, tpl) {
    const company = e.currentTarget.value;
    // console.log(company);
    $('[name=companyNew]').val(company);
    $('[name=companyId]').val('');
  },

  'autocompleteselect input': function(e, tpl, doc) {
    // console.log("selected ", doc);
    $('[name=companyId]').val(doc._id);
    $('[name=companyNew]').val('');
  },

  'keyup [name=company-filter]': function(e, tpl) {
    tpl.filterName.set(e.currentTarget.value);
  },

  'click .edit': function(e, tpl) {
    tpl.companyEditId.set(this._id);
  },

  'click .delete': function() {
    const that = this;
    const translation = TAPi18n.__('deleteConfirmationMessage');
    bootbox.confirm(translation, function(result) {
      if(!result) return;
      Meteor.call('adminCompanyRemove', that._id);
    });
  },

  'submit #add': function(e, tpl) {
    e.preventDefault();

    const form = tpl.$(e.target).serializeJSON();

    let errors = {};
    if(!form.companyNew && !form.companyId) errors.company = "company Required";
    if(Object.keys(errors).length) return Session.set('postSubmitErrors', errors);
    else Session.set('postSubmitErrors', {});

    $('#add').hide();
    $('#wait').show();

    Meteor.call('adminCompaniesHrAdd', form, function(err) {
      if(!err) {
        $('#add').show();
        $('#wait').hide();
      }
    });
  }
});

//
// /**
//  * Edit
//  */
//
// Template.pollbizAdminCompanyEdit.onCreated(function() {
//   this.companyEditId = Blaze.currentView.parentView.parentView.templateInstance().companyEditId;
//   Session.set('postSubmitErrors', {});
//
// });
//
// Template.pollbizAdminCompanyEdit.helpers({
//
//   editedCompany() {
//     return Companies.findOne(Template.instance().companyEditId.get());
//   },
// });
//
//
// Template.pollbizAdminCompanyEdit.events({
//
//   'click .cancel': function(e, tpl) {
//     tpl.companyEditId.set(0);
//   },
//
//   'submit #edit': function(e, tpl) {
//     e.preventDefault();
//
//     let c = tpl.$('form').serializeJSON();
//     let errors = {};
//     if(!c.name) errors.name = "Name Required";
//
//     if(Object.keys(errors).length) return Session.set('postSubmitErrors', errors);
//     else Session.set('postSubmitErrors', {});
//
//     Meteor.call('adminCompanyUpdate', this._id, c, function(err) {
//       if(!err) {
//         tpl.companyEditId.set(0)
//       }
//     });
//   }
// });